/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Desktop.Components.Dialog.TaskPanels;

import Core.View.TaskDialogs.TaskDialog;

/**
 *
 * @author Ramclen
 */
public interface TaskPanel {
    public abstract TaskDialog getTaskDialog();
}
