package Desktop.Components.MainWindow;

import Core.TaskManager.Event;
import Core.TaskManager.EventPanelListener;
import Core.TaskManager.UtilTime;
import Core.View.DiaryViewer;
import java.awt.Color;
import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

public class DiaryPanel extends JPanel{
    
    private EventPanelListener eventPanelListener;

    public DiaryPanel(EventPanelListener eventPanelListener) {
        this.eventPanelListener = eventPanelListener;
    }
     
    public void addEvent(Event event) {
        try {
            getEventPanel(event);
        } catch (ClassNotFoundException ex) {
            add(new EventPanelSwing(event, eventPanelListener));
        }
    }
    
    public void create(){
        setLayout(new BoxLayout(this,  BoxLayout.Y_AXIS));
        setBorder(new TitledBorder("Tasks") );
    }
    
    public void removeAllEvents(){
        removeAll();
    }
    
    public EventPanelSwing getEventPanel(Event event) throws ClassNotFoundException {
        for (Component component: getComponents()) {
            EventPanelSwing eventPanel = (EventPanelSwing)component;
            if(eventPanel.getEvent().equals(event))
                return eventPanel;
        }
        throw new ClassNotFoundException("EventPanel "+ event.getName()+" not present in diaryPanel");
    }

    public void removeEvent(Event event) {
        try {
            remove(getEventPanel(event));
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DiaryPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
