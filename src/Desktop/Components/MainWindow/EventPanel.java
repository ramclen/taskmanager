package Desktop.Components.MainWindow;

import Core.TaskManager.Event;
import Core.TaskManager.Observer.Observer;
import java.awt.Color;
import static java.awt.Frame.MAXIMIZED_HORIZ;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class EventPanel extends JPanel{
    private Event event;
    private JLabel countDownLabel;
    
    public EventPanel(Event event ) {
        this.event = event;        
        countDownLabel = new JLabel();
        create();
    }
    
    public void create(){
        add(new JLabel(event.getDescription()));
        add(new JLabel(event.getTime().toString()));
        add(countDownLabel);
        setBorder(new TitledBorder(event.getName()));
    }
    
    public Event getEvent(){
        return event;
    }

    public void setCountDown(String time) {
        countDownLabel.setText(time);
    }
    
    
}
