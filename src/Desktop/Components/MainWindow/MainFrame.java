package Desktop.Components.MainWindow;

import Core.View.DiaryViewer;
import Desktop.Components.Dialog.AddEventJDialog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;

public class MainFrame extends JFrame{
    private JPanel mainPanel;
    public MainFrame(DiaryPanel diaryPanel) {
        diaryPanel.create();
        setLayout(new FlowLayout());
        setContentPane(diaryPanel);        
    }
    
    public void create(){
        JButton button = new JButton("Add Event");
        
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                addEventButtonActionPerformed(evt);
            }
        });
        
        setContentPane(button);
        setTitle("TaskManager");
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(500, 500);   
    }
    
    private void addEventButtonActionPerformed(ActionEvent evt) {
        new AddEventJDialog(this, true, null).setVisible(true);
    }
}
