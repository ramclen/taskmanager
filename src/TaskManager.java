

import Core.Controller.AddEventCommand;
import Core.Controller.CloseEventCommand;
import Core.Controller.EventsController;
import Core.TaskManager.EventPanelListener;
import Core.TaskManager.Diary;
import Core.TaskManager.Listener;
import Core.View.AddEventDialog;
import Core.View.DiaryViewer;
import Core.View.EventViewer;
import Desktop.Components.MainWindow.DiaryPanel;
import Desktop.Components.MainFrameSwing;
import Desktop.Components.MainWindow.EventPanel;
import Desktop.Components.MainWindow.EventPanelSwing;

public class TaskManager {
    static Listener addCommandListener = new Listener() {
            @Override
            public void change() {
                new AddEventCommand(new AddEventDialog(window.getAddEventDialog()), diary).execute();
            }
        };
    static EventPanelListener closeCommandListener = new EventPanelListener() {

        @Override
        public void Change(EventPanelSwing panel) {
            new CloseEventCommand(new EventViewer(panel), diary).execute();
        }
    };
    
    static Diary diary = new Diary();
    static DiaryPanel diaryPanel = new DiaryPanel(closeCommandListener);
    static MainFrameSwing window = new MainFrameSwing(diaryPanel, addCommandListener, closeCommandListener);
    static DiaryViewer diaryViewer = new DiaryViewer(diary.getEventList(), diaryPanel);
    static EventsController controller = new EventsController(diary, diaryViewer);
    
    
    public static void main(String[] args) throws InterruptedException {
        window.setVisible(true);
    }
}
