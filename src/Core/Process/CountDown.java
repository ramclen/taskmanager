
package Core.Process;

import java.util.logging.Level;
import java.util.logging.Logger;
import Core.TaskManager.Observer.ObservableCountDown;
import Core.TaskManager.Time;

public class CountDown extends ObservableCountDown implements Runnable{
    private Time time;
    private boolean life;
    
    public CountDown(Time time) {
        super();
        this.time = time;
        life = true;
    }
    
    @Override
    public void run(){
        while(!isFinal() && life){
           if(time.getSeconds()!=0){
                countDownWait(1000);
                time.decresesSecond();
                
            }else{
                countDownWait(1000*60);
                time.decressMinutes();
            }
            notifyTimeChange(time);
        }
        if(life)
            notifyChanges();
        life=false;
    }
    
    public boolean isAlive(){
        return life;
    }
    
    public void kill(){
        life = false;
    }
    
    private boolean isFinal(){
        boolean finalSeconds= time.getSeconds()==0;
        boolean finalMinutes = time.getMinutes()==0;
        return (time.getHours()==0) && finalMinutes && finalSeconds;
    }
    
    private void countDownWait(int milis){
        try {
            Thread.sleep(milis);
        } catch (InterruptedException ex) {
            Logger.getLogger(CountDown.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
}
