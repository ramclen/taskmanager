
package Core.View;

import Core.TaskManager.Event;
import Desktop.Components.MainWindow.DiaryPanel;
import Desktop.Components.MainWindow.EventPanelSwing;
import java.awt.Component;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DiaryViewer {
    private List<Event> eventList;
    private DiaryPanel diaryPanel;
    private int oldSizeList;
    
    public DiaryViewer (List eventList, DiaryPanel diaryPanel){
        this.diaryPanel = diaryPanel;
        this.eventList= eventList;
        oldSizeList = eventList.size();
    }
    public void setEventList(List eventList){
        if(oldSizeList>eventList.size())
            clearEvents();
        oldSizeList=eventList.size();
        refresh();
    }
    
    public EventViewer getEventViwer(Event event) throws ClassNotFoundException{
        return new EventViewer(diaryPanel.getEventPanel(event));
    }
    
    public void refresh(){
        for (Event event: eventList) 
            diaryPanel.addEvent(event);
        diaryPanel.updateUI();
    }

    private void clearEvents() {
        Component[] components = diaryPanel.getComponents();
        for (Component component : components)
            isInDiaryPanel(component);
        diaryPanel.updateUI();
    }

    private void isInDiaryPanel(Component component) {
        if(component instanceof EventPanelSwing){
            EventPanelSwing panel = (EventPanelSwing) component;
            if (!eventList.contains(panel.getEvent()))
                diaryPanel.remove(component);
        }
    }
    
}
