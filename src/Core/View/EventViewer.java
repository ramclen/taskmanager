
package Core.View;

import Core.TaskManager.Event;
import Core.TaskManager.Observer.Observable;
import Core.TaskManager.Observer.Observer;
import Core.TaskManager.Observer.TimeObserver;
import Core.TaskManager.Time;
import Desktop.Components.MainWindow.EventPanel;
import Desktop.Components.MainWindow.EventPanelSwing;
import java.awt.Color;

public class EventViewer implements TimeObserver{
    private EventPanelSwing eventPanel;
    
    public EventViewer(EventPanelSwing eventPanel) {
        this.eventPanel = eventPanel;
    }
    
    public Event getEvent(){
        return eventPanel.getEvent();
    }
    
    @Override
    public void update() {
        eventPanel.setBackground(Color.red);
    }
    
    @Override
    public void timeChange(Time time) {
        eventPanel.setCountDown(time.toString());
    }
    
}
