/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core.View.TaskDialogs;

import Core.TaskManager.Task.BrowserTask;
import Core.TaskManager.Task.Task;
import Desktop.Components.Dialog.TaskPanels.BrowserPanel;

/**
 *
 * @author Ramclen
 */
public class BrowserTaskDialog implements TaskDialog{
    
    private BrowserPanel panel;

    public BrowserTaskDialog(BrowserPanel panel) {
        this.panel = panel;
    }
    
    @Override
    public Task getTask() {
        return new BrowserTask("http://"+panel.getURL());
    }
    
}
