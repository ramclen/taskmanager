
package Core.View.TaskDialogs;

import Core.TaskManager.Task.NotificationTask;
import Core.TaskManager.Task.Task;
import Desktop.Components.Dialog.TaskPanels.NotificationPanel;
import Desktop.Components.MainWindow.EventPanel;

public class NotificationTaskDialog implements TaskDialog{
    private NotificationPanel notificationPanel;

    public NotificationTaskDialog(NotificationPanel notificationPanel) {
        this.notificationPanel = notificationPanel;
    }
    
    @Override
    public Task getTask() {
        return new NotificationTask(notificationPanel.getMessage());
    }
    
}
