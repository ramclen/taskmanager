/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core.View.TaskDialogs;

import Core.TaskManager.MusicPlayer.JLMusicPlayer;
import Core.TaskManager.Task.MusicTask;
import Core.TaskManager.Task.Task;
import Desktop.Components.Dialog.TaskPanels.MusicPanel;

/**
 *
 * @author Ramclen
 */
public class MusicTaskDialog implements TaskDialog{
    private MusicPanel musicPanel;

    public MusicTaskDialog(MusicPanel musicPanel) {
        this.musicPanel = musicPanel;
    }
    
    @Override
    public Task getTask(){
        return new MusicTask(musicPanel.getSong(), new JLMusicPlayer(), musicPanel.getRepeat());
    }
}
