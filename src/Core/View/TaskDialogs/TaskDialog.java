/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core.View.TaskDialogs;

import Core.TaskManager.Task.Task;

/**
 *
 * @author Ramclen
 */
public interface TaskDialog {
    public abstract Task getTask();
}
