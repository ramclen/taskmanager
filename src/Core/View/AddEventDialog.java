
package Core.View;

import Core.View.TaskDialogs.TaskDialog;
import Core.TaskManager.Event;
import Core.TaskManager.Time;
import Desktop.Components.Dialog.AddEventJDialog;

public class AddEventDialog {
    AddEventJDialog addEventJDialog;

    public AddEventDialog(AddEventJDialog addEventJDialog) {
        this.addEventJDialog = addEventJDialog;
    }
    
    public Event getEvent(){
        TaskDialog taskDialog =addEventJDialog.getTaskPanel().getTaskDialog();
        String name = addEventJDialog.getName();
        String description = addEventJDialog.getDescription();
        String uri = addEventJDialog.getUriImage();
        return new Event(name, description, getTime(), taskDialog.getTask(), uri);
    }
    
    private Time getTime(){
        int hour = Integer.valueOf(addEventJDialog.getHour());
        int minutes = Integer.valueOf(addEventJDialog.getMinutes());
        int seconds = Integer.valueOf(addEventJDialog.getSeconds());
        return new Time(hour, minutes, seconds);
    }
}
