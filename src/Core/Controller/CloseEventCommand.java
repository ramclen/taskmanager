
package Core.Controller;

import Core.TaskManager.Diary;
import Core.TaskManager.Event;
import Core.View.EventViewer;


public class CloseEventCommand extends Command{
    private EventViewer eventViewer;
    private Diary diary;

    public CloseEventCommand(EventViewer eventViewer, Diary diary) {
        this.diary= diary;
        this.eventViewer = eventViewer;
    }
    
    
    
    @Override
    public void execute() {
        Event event = eventViewer.getEvent();
        event.stopEvent();
        diary.closeEvent(eventViewer.getEvent());
    }
    
}
