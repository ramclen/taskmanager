
package Core.Controller;

import Core.Process.CountDown;
import Core.TaskManager.Diary;
import Core.TaskManager.Event;
import Core.TaskManager.Observer.Observer;
import Core.TaskManager.Time;
import Core.TaskManager.UtilTime;
import Core.View.DiaryViewer;
import Core.View.EventViewer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CountDownController implements Observer{
    
    private DiaryViewer diaryViewer;
    private CountDown countDown;
    private Diary diary;

    public CountDownController(Diary diary, DiaryViewer diaryViewer) {
        this.diary = diary;
        this.diaryViewer = diaryViewer;
        countDown = new CountDown(null);
    }
    
    public void executeCountDown(Event event) {
        if(countDown.isAlive())
            countDown.kill();
        
        Time time = UtilTime.timeToFinish(event.getTime());
        countDown = new CountDown(time);
        countDown.add(this);
        try {
            EventViewer eventViewer = diaryViewer.getEventViwer(event);
            countDown.addTimeObserver(eventViewer);
            countDown.add(eventViewer);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EventsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        new Thread(countDown).start();
    }

    @Override
    public void update() {
        diary.getEvent().startEvent();
        if(diary.prepareNextEvent()){
            executeCountDown(diary.getEvent());
        }
    }
}
