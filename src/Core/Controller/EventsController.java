
package Core.Controller;

import Core.TaskManager.Listener;
import Core.TaskManager.Diary;
import Core.TaskManager.Event;
import Core.View.DiaryViewer;
public class EventsController implements Listener{
    
    private Diary diary;
    private DiaryViewer diaryViewer;
    private final CountDownController countDownController;
    private Event eventControlled;
    
    public EventsController(Diary diary, DiaryViewer diaryViewer) {
        this.diary = diary;
        this.diaryViewer = diaryViewer;
        countDownController = new CountDownController(diary, diaryViewer);
        diary.setListener(this);
    }
    
    public void start(){
        if(diary.getEventList().size()!=0)
            countDownController.executeCountDown(diary.getEvent());
    }
    
    @Override
    public void change(){
        diaryViewer.setEventList(diary.getEventList());
        if(diary.getEvent()!=eventControlled){
            eventControlled = diary.getEvent();
            countDownController.executeCountDown(eventControlled);
        }
        
    }

}
