/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core.Controller;

import Core.TaskManager.Diary;
import Core.View.AddEventDialog;

public class AddEventCommand extends Command{
    private AddEventDialog addEventDialog;
    private Diary diary;
    
    public AddEventCommand(AddEventDialog addEventDialog, Diary diary) {
        this.addEventDialog = addEventDialog;
        this.diary = diary;
    }
    
    @Override
    public void execute() {
        diary.addEvent(addEventDialog.getEvent());
    }
    
}
