/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core.TaskManager;

import Desktop.Components.MainWindow.EventPanel;
import Desktop.Components.MainWindow.EventPanelSwing;

public interface EventPanelListener {
    public abstract void Change(EventPanelSwing panel);
}
