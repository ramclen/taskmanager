package Core.TaskManager.Task;

import javax.swing.JOptionPane;

public class NotificationTask implements Task{
    
    private String message;

    public NotificationTask(String message) {
        this.message = message;
    }
    
    @Override
    public void execute() {
        JOptionPane.showMessageDialog(null, message);
    }

    @Override
    public void stopExecution() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
