/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Core.TaskManager.Task;

public interface Task {
    public abstract void execute();
    public abstract void stopExecution();
}
