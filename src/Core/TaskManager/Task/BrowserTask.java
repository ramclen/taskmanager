
package Core.TaskManager.Task;

import edu.stanford.ejalbert.BrowserLauncher;
import edu.stanford.ejalbert.exception.BrowserLaunchingInitializingException;
import edu.stanford.ejalbert.exception.UnsupportedOperatingSystemException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BrowserTask implements Task{
    private String URL;

    public BrowserTask(String URL) {
        this.URL = URL;
    }
    
    @Override
    public void execute() {
        BrowserLauncher launcher;
        try {
            launcher = new BrowserLauncher();
            launcher.openURLinBrowser(URL);
        } catch (BrowserLaunchingInitializingException ex) {
            Logger.getLogger(BrowserTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedOperatingSystemException ex) {
            Logger.getLogger(BrowserTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void stopExecution() {
        throw new UnsupportedOperationException("Not Supported this operation in Browsers Tasks");
    }
    
}
