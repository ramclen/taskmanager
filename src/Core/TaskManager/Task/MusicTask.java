
package Core.TaskManager.Task;
import Core.TaskManager.MusicPlayer.MusicPlayer;

public class MusicTask implements Task{
    
    private MusicPlayer player;
    private String song;
    
    public MusicTask(String song, MusicPlayer player, boolean repeat) {
        this.player = player;
        this.song = song;
        player.setRepeat(repeat);
    }
    
    @Override
    public void execute() {
        player.turnOn(song);
    }

    @Override
    public void stopExecution() {
        player.turnOff();
    }
    
    
}
