
package Core.TaskManager;

import java.util.Date;

public class Time{
    private int hours;
    private int minutes;
    private int seconds;
    
    public Time(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }
    
    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }
    
   
    
    public void decresesSecond(){
        if(seconds==0){
            seconds=59;
            decressMinutes();
        }else
            seconds--;
    }

    public void decressMinutes() {
        if(minutes==0){
            minutes=59;
            decressHours();
        }else
            minutes--;
    }

    public void decressHours() {
        if(hours!=0){
            hours--;
        }else{
            minutes=0;
            seconds=0;
        }
    }
    
    @Override
    public String toString(){
        return hours +":" + minutes +":" + seconds;
    }
    
}
