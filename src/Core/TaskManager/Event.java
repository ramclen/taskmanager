
package Core.TaskManager;

import Core.TaskManager.Task.Task;
import java.awt.Image;
import javax.swing.ImageIcon;

public class Event implements Comparable<Event>{
    private Time time;
    private Task task;
    private String name;
    private String description;
    private Image image;

    public Event(String name, String description, Time time, Task task, String uriImage) {
        this.name = name;
        this.description = description;
        this.time = time;
        this.task = task;
        this.image = new ImageIcon(uriImage).getImage(); 
    }
   
    public void startEvent(){
        task.execute();
    }
    
    public void stopEvent(){
        task.stopExecution();
    }

    @Override
    public int compareTo(Event event) {
        if(UtilTime.isBiggest(time, event.getTime())){
            return 1;
        }else if (UtilTime.isEquals(event.getTime(), time)) {
            return 0;
        }else{
            return -1;
        }
    }
    
    public String getName() {
        return name;
    }
        
    public Time getTime() {
        return time;
    }

    public Task getTask() {
        return task;
    }
    
    public String getDescription() {
        return description;
    }

    public Image getImage() {
        return image;
    }
    
}
