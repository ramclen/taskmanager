
package Core.TaskManager;

import java.util.Calendar;


public class UtilTime {
    
    public static Time timeToFinish(Time finishTime){
        return timeSubstraction(finishTime, getCurrentTime());
    }
    
    public static Time timeSubstraction(Time minuend, Time substrahend){
        int difSeconds= minuend.getSeconds() - substrahend.getSeconds();
        int difMinutes= minuend.getMinutes() - substrahend.getMinutes();
        int difHour= minuend.getHours()- substrahend.getHours();
        return getAbsTime(difHour, difMinutes, difSeconds);
    }
    
    private static Time getAbsTime(int hour, int minutes, int seconds) {
        if(seconds<0){
            minutes--;
            seconds+=60;
        }
        if(minutes<0){
            hour--;
            minutes+=60;
        }
        if(hour<0)
            hour+=24;
        return new Time(hour, minutes, seconds);
    }
    
    public static Time getCurrentTime(){
        Calendar calendar = Calendar.getInstance();
        return new Time( calendar.get(Calendar.HOUR_OF_DAY), 
                         calendar.get(Calendar.MINUTE),
                         calendar.get(Calendar.SECOND));
    }
    
    public static boolean isBiggest (Time bigger, Time smaller){
        int result = getInSecond(bigger) - getInSecond(smaller);
        if(result>0)
            return true;
        return false;
    }
    
    public static int getInSecond(Time time){
        return time.getHours()*3600 + time.getMinutes()*60 + time.getSeconds();
    }
    
    public static boolean isEquals(Time time1, Time time2) {
        if(time2.getHours()==time1.getHours())
            if(time2.getMinutes()==time1.getMinutes())
                if(time2.getSeconds()==time1.getSeconds())
                    return true;
        return false;
    }
    
}
