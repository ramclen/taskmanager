

package Core.TaskManager.Observer;

import Core.TaskManager.Time;
import java.util.ArrayList;

public abstract class ObservableCountDown extends Observable{
    private ArrayList<TimeObserver> timeObserverList;

    public ObservableCountDown() {
        timeObserverList = new ArrayList<TimeObserver>();
    }
    
    public void addTimeObserver(TimeObserver observer){
        timeObserverList.add(observer);
    }
    
    public void notifyTimeChange(Time time){
        for (TimeObserver observer : timeObserverList)
            observer.timeChange(time);
    }
    
    
}
