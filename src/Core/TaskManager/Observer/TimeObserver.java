
package Core.TaskManager.Observer;

import Core.TaskManager.Time;

public interface TimeObserver extends Observer{
    public abstract void timeChange(Time time);
}
