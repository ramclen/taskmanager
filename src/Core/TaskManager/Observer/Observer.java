
package Core.TaskManager.Observer;

import java.util.ArrayList;

public interface Observer{
    public abstract void update();
}
