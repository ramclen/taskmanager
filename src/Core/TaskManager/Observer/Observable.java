
package Core.TaskManager.Observer;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable extends ArrayList<Observer>{
    
    public void notifyChanges(){
        for (Observer observer : this) {
            observer.update();
        }
    }
    
}
