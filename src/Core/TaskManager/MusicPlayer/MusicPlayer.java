
package Core.TaskManager.MusicPlayer;

public interface MusicPlayer {
    public abstract void turnOn(String uri);
    public abstract void turnOff();
    public void setRepeat(boolean repeat);
}
