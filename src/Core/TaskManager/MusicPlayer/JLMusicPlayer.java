
package Core.TaskManager.MusicPlayer;

import Core.TaskManager.Task.MusicTask;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class JLMusicPlayer extends Thread implements MusicPlayer{
    private Player player;
    private String song;
    private boolean repeat;

    public JLMusicPlayer() {
        song = "";
        repeat=false;
    }
    
    @Override
    public void run(){
        try {            
            do{
                player = new Player(new FileInputStream(song));
                player.play();
            }while(repeat);
            
        } catch (Exception ex) {
            Logger.getLogger(JLMusicPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void turnOn(String uri){
        song = uri;
        start();
    }
    
    @Override
    public void turnOff(){
        if(player!=null)
            player.close();
        repeat = false;
    }

    @Override
    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }
    
    
}
