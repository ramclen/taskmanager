package Core.TaskManager;

import java.util.List;

public class Diary {
    private BinaryEventList binaryEventList;
    private Listener listener;
    private int pivot;

    public Diary() {
        binaryEventList = new BinaryEventList();
        pivot=0;
    }
    
    public void addEvent(Event event){
        calculatePivot(binaryEventList.add(event));
        fireListener();
    }
    
    

    private void calculatePivot(int newEventPosition) {
        if(binaryEventList.size()==0){
            pivot=0;
            return;
        }
            
        if(pivot-1==newEventPosition)
            if(isBiggerThanCurrentTime(newEventPosition)){
               pivot=newEventPosition; 
               return;
            }
        if(pivot==newEventPosition)
            if(!isBiggerThanCurrentTime(newEventPosition)){
                if(pivot+1<binaryEventList.size())
                    pivot++;
                return;
            }
        if(pivot+1==newEventPosition)
            if(!isBiggerThanCurrentTime(pivot))
                pivot= newEventPosition;
    }
    
    private void fireListener() {
        if(listener!=null)
            listener.change();
    }
    
    private boolean isBiggerThanCurrentTime(int position) {
        return  UtilTime.isBiggest(binaryEventList.get(position).getTime(), UtilTime.getCurrentTime());
    }

    private boolean isCloseAndLowToPivotTime(int position) {
        return pivot-1==position || pivot==position;
    }
    
    /*calcular cercania con el tiempo actual
    private boolean isClosest(Time time, Time time2){
        int result = UtilTime.timeToFinish(time)-UtilTime.timeToFinish(time2);
    }
    */
    public boolean prepareNextEvent() {
        pivot++;
        return pivot<binaryEventList.size();
    }
    
    public Time getCurrentEventTime() {
        return UtilTime.timeToFinish(binaryEventList.get(pivot).getTime());
    }
    
    public List getEventList() {
        return binaryEventList.getList();
    }
    
    public Event getEvent(){
        return binaryEventList.get(pivot);
    }
    
    public void setListener(Listener listener){
        this.listener = listener;
    }

    public void closeEvent(Event event) {
        binaryEventList.remove(event);
        fireListener();
    }

    
}
