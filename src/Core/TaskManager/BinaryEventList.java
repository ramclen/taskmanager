package Core.TaskManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BinaryEventList{
    private ArrayList<Event> list;

    public BinaryEventList() {
        list = new ArrayList<Event>();
    }
    
    //TODO Binary Insertion 
    public int add(Event event){
        list.add(event);
        Collections.sort(list);
        return list.indexOf(event);
    }
    
    public Event get(int index) {
        return list.get(index);
    }

    public int size() {
        return list.size();
    }

    public List getList() {
        return list;
    }

    void remove(Event event) {
        list.remove(event);
    }
    
    
        
}
