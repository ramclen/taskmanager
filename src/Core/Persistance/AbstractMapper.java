
package Core.Persistance;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

abstract class AbstractMapper {

    protected Map loadedMap = new HashMap();
    protected DataBaseHandler DB;
    protected String query;

    public AbstractMapper() {
        DB = new DataBaseHandler();
    }
    
    protected Object abstractFind(long id) {
        Object result = loadedMap.get(id);
        if (result != null) {
            return result;
        }

        List list = DB.launchQuery(query);       
        result = load(list);
        return result;
    }

    protected Object load(List list) {
        
        long id = Long.valueOf(list.get(0).toString());
        
        Object result = loadedMap.get(id);
        if (result != null) {
            return result;
        }

        result = doLoad(list);
        loadedMap.put(id, result);
        return result;
    }
    
    protected Object Select(){
        return doLoad(DB.launchQuery(query));
    } 
    
    abstract protected String findStatement();
    
    abstract protected Object doLoad(List list);

    abstract protected Object Update();

    abstract protected Object Insert(Object register);

    abstract protected Object Delete();
    
    
    

}
