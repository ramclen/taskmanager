/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core.Persistance;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataBaseHandler {
    private final String db = "shopmusic";
        
    public Connection getConnection(){
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataBaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        String url = "jdbc:sqlite://" + db;
        try {
            return DriverManager.getConnection(url);
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public Statement getStatement(Connection connection){
        try {
            Statement statement = connection.createStatement();
            return statement;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseHandler.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public List launchQuery(String query){
        Statement statement = getStatement(getConnection());
        try {
            return ResultSetToList(statement.executeQuery(query));            
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public ResultSet launchQueryRs(String query){
         Statement statement = getStatement(getConnection());
         try {
            return statement.executeQuery(query);
         } catch (SQLException ex) {
            Logger.getLogger(DataBaseHandler.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
    }
    
    private List ResultSetToList(ResultSet set) {
        try {
            List list = new ArrayList();
            int colLimit = set.getMetaData().getColumnCount();
            while (set.next()) 
                for (int i = 1; i < colLimit+1; i++) 
                    list.add(String.valueOf(set.getObject(i)));
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List launchInsert(String query){
        Statement statement = getStatement(getConnection());
        try {
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            return ResultSetToList(statement.getGeneratedKeys());
        } catch (SQLException ex) {
            Logger.getLogger(DataBaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
